<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        Db::table('users')->insert([
        	[
        		'name' => 'Arun',
        		'email'	=> 'arun@arun.com',
        		'password' => bcrypt('secret')	
        	],
        	[
        		'name' => 'Sri',
        		'email'	=> 'sri@sri.com',
        		'password' => bcrypt('secret')	
        	]
        	]);
    }
}
