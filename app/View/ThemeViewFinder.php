<?php 

namespace LaraCms\View;

use Illuminate\View\FileViewFinder;

class ThemeViewFinder extends FileViewFinder
{
	protected $activeTheme;

	protected $basePath;

	public function setbasePath($path)
	{
		$this->basePath = $path;
	}

	public function setActiveTheme($theme)
	{
		$this->activeTheme = $theme;

		array_unshift($this->paths, $this->basePath.'/'.$theme.'/views');
	}
}