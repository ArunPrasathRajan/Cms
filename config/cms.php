<?php

return [

	'theme' => [

		'folder' => 'themes',
		'active' => 'default'
	],

	'templates' => [

		'page' => LaraCms\Templates\PageTemplate::class

	]

];