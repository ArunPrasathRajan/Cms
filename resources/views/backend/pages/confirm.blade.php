@extends('layouts.backend')

@section('title', 'Delete '.$page->title)

@section('content')
	{!! Form::open(['method' => 'delete', 'route' => ['backend.pages.destroy', $page->id]]) !!}
		<div class="alert alert-danger">
			<strong>Warning!</strong> You are going to delete this page.
		</div>

		{!! Form::submit('Yes, delete this page!', ['class' => 'btn btn-danger']) !!}

		<a href="{{ route('backend.pages.index') }}" class="btn  btn-success">
			<strong> Go back!!</strong>
		</a>
	{!! Form::close() !!}
@endsection